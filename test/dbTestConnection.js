const dbTest = require('@prejt/db-test');
const path = require('path');

const dbAdapter = 'sql';

const connectionInfo = {
	host: 'localhost',
	user: 'root',
	password: 'password',
	database: 'db'
};

const init = async (ctx) => {
	ctx.timeout(20000);
	const initialized = await dbTest.init(path.join(__dirname, '..', 'test-db'), connectionInfo, false);
}

const start = async (ctx) => {

	const paths = [__dirname + '/../test-models'];
	const storageConfigs = await loadStorageConfigs(paths);

	switch (dbAdapter) {
		case 'sql':
			ctx.timeout(20000);
			ctx.container = await dbTest.start();
			ctx.connection = sqlAdapter.getDbConnection(connectionInfo);
			const sqlAdapterInstance = sqlAdapter.createAdapter(ctx.connection);
			ctx.getStorage = await getStorageFactory(storageConfigs, [], sqlAdapterInstance);
			break;
		default:
			const mockAdapterInstance = mockAdapter.createAdapter({});
			ctx.getStorage = await getStorageFactory(storageConfigs, [], mockAdapterInstance);
			break;
	}
}

const stop = async (ctx) => {
	if (dbAdapter === 'sql') {
		await dbTest.remove(ctx.container);
	}
}

module.exports = {
	init,
	start,
	stop
};